const path = require('path')
const less = require('postcss-less-engine');

module.exports = {
    parser: less.parser,
    plugins: [
        less(), 
        // require("postcss-import")({
        //     path:[path.resolve(process.cwd(), './src')],
        // }),
        // require('postcss-nested'),
        // require('postcss-less-vars')(),
        require('autoprefixer'),
    ]
}