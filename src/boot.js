import _ from 'underscore'; 
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {create, history} from './store';
import assignDeep from 'deep-assign';

export default function Boot(App){

    var store = create();

    ReactDOM.render(
        <Provider store={store}><App history={history} store={store}/></Provider>
        ,document.getElementById('root')
    );

}
