import _ from 'underscore'
import request from 'superagent'
import queryString from 'query-string'

const NS = '@app/'

const query = queryString.parse(window.location.search)

export const initialState = {
    someVar: '',
}


export default function reducer(state = initialState, action = {}){
    switch (action.type) {

        case EXAMPLE_SET:
            return {...state, ...action.payload}

        default:
            return state
    }
}




export function exampleGet(callback){
    return (dispatch, getState)=>{

        request.get(`/service/Apps/MonitoringService/Monitor/Data`)
        .set('Accept', 'text/html')
        .end((error, res)=> {
           dispatch(exampleSet(res.text))
        })

    }
}

export const EXAMPLE_SET = NS+'EXAMPLE_SET'
export function exampleSet(data){
    return {
        type: EXAMPLE_SET,
        payload:{
            someVar:data
        }
    }
}