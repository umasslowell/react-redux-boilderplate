import _ from 'underscore';
import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

import * as reducers from './redux';

export var store = null;
export var history = null;

export function createReducer(asycReducers){
    return combineReducers({
        ...reducers,
        ...asycReducers,
    })
}

export function injectAsyncReducer(name, asyncReducer) {
    store.asyncReducers[name] = asyncReducer;
    store.replaceReducer(create(store.asyncReducers));
}

export function create(initialState = {}) {

    var newStore = createStore(
        createReducer(),
        initialState, 
        compose(
            applyMiddleware(
                thunkMiddleware,
            ),
            (typeof window !== 'undefined' && window.devToolsExtension) ? window.devToolsExtension() : f => f
        )
    )
    
    newStore.asyncReducers = {};
    store = newStore;
    return store
}