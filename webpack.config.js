var path = require('path');
var fs = require('fs');
var _ = require('underscore');
var pkg = require('./package.json');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var LiveReloadPlugin = require('webpack-livereload-plugin');
var autoprefixer = require('autoprefixer');

var argv = require('minimist')(process.argv.slice(2));

module.exports = function(env, config, options){

    env = env || {};

    var plugins = [
        new ExtractTextPlugin({ 
            filename:'[name]'+(argv.debug?'.dev':'')+'.css',
            allChunks: true,
        }),
    ];

    if(argv.debug !== true){
        plugins.push(new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }));
        plugins.push(new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
        }));
    }else{
        plugins.push(new LiveReloadPlugin({
            port: 4000 + (Math.round(50*Math.random())),
            appendScriptTag:true
        }));
    }

    return {
        entry: {
            'app':"./src/index.js",
        },
        output: {
            path: './dist',
            filename: "[name]"+(argv.debug?'.dev':'')+".js",
            chunkFilename: "[name].js",
            publicPath: './dist/',
        },
        devtool:'eval-source-map',
        module: {
            loaders: [
                { test: /\.js$/, exclude: /(node_modules|bower_components)/, loader: "babel-loader", query:{compact: false} },
                { test: /\.js$/, include: /(es6)/ , loader: "babel-loader", query:{compact: false} },
                { test: /\.json$/, loader: "json-loader" },
                { test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/, loader: 'file-loader?name=[name].[ext]' },
                {
                    test: /\.less$/,
                    loader:ExtractTextPlugin.extract({
                        loader:[
                            {
                                loader:'css-loader',
                                query:{sourceMap:true, importLoaders:1}
                            },
                            {
                                loader:'autoprefixer-loader',
                            },
                            {
                                loader:'less-loader',
                            },
                            {
                                loader:'./webpack/concat.js',
                                query:{
                                    append:`\n@import "${path.resolve('./src/variables.less')}";`
                                }
                            }
                        ]
                    })
                },
                // {
                //     test: /\.css$/,
                //     loader:ExtractTextPlugin.extract({
                //         loader:[
                //             {
                //                 loader:'css-loader',
                //                 query:{sourceMap:true, importLoaders:1}
                //             },
                //         ]
                //     })
                // },
            ]
        },
        plugins:plugins,
        resolve: {
            alias: {
                "src": path.resolve(process.cwd(), './src'),
            },
        },
    };

}



