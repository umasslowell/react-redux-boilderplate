var loaderUtils = require('loader-utils')

module.exports = function(source, map) {
    this.cacheable()
    var query = loaderUtils.parseQuery(this.query)
    var append = query.append || ''
    var prepend = query.prepend || ''

    if(prepend !== ''){
        source = prepend.slice(1) + '\n' + source
    }

    if(append !== ''){
        source += '\n'+append.slice(1)
    }

    this.callback(null, source, map)
};